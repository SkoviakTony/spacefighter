#include "Boss01.h"


Boss01::Boss01()
{
	SetSpeed(20);
	SetMaxHitPoints(60);
	SetCollisionRadius(80);
}


void Boss01::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void Boss01::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		if(GetHitPoints() > 20)
		{
			pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, 0, 1);
		}
		else
		{
			pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Red, m_pTexture->GetCenter(), Vector2::ONE, 0, 1);
		}
	}
}
