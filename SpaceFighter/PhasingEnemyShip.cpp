#include "PhasingEnemyShip.h"


PhasingEnemyShip::PhasingEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void PhasingEnemyShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		int seconds = int(pGameTime->GetTotalTime());
		
		if(seconds % 3 == 0)
		{
			SetInvulnurable();
		}
		else
		{
			SetInvulnurable(false);
		}
		
		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void PhasingEnemyShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);

		if (IsInvulnurable())
		{
			pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::Red, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
		}
	}
}
