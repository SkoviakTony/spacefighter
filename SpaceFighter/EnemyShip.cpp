
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	// If there is a positive delay value, subtract the elapsed time from it.
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		// Once delay - elapsed time is zero or less, activate the enemy ship.
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		// Count the elapsed time that the ship has been active (?)
		m_activationSeconds += pGameTime->GetTimeElapsed();
		// If the ship has been active for more than 2 seconds (?) and is not on the screen, then deactivate it
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	// Update enemy ship activation again
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}