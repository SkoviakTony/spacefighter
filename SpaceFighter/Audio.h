
#pragma once

#include "KatanaEngine.h"

class Audio
{
public:
	Audio();
	~Audio();
	virtual void LoadSample(const char path[]);
	virtual void PlaySound(int sound_type);

private:
	ALLEGRO_SAMPLE* player_gun;
	ALLEGRO_SAMPLE* hit;
	ALLEGRO_SAMPLE* song;
	ALLEGRO_SAMPLE_INSTANCE* song_instance;
};

