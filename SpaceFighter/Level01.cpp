

#include "Level01.h"
#include "BioEnemyShip.h"
#include "Boss01.h"
#include "PhasingEnemyShip.h"

void Level01::LoadContent(ResourceManager *pResourceManager)	
{	
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture* pBossTexture = pResourceManager->Load<Texture>("Textures\\Boss01.png");

	// Loop level01's theme as long as the game is on level 01
	al_set_sample_instance_playmode(level01_instance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(level01_instance, al_get_default_mixer());

	al_play_sample_instance(level01_instance);
	
	const int COUNT = 31;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < 16; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	for (int i = 10; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		PhasingEnemyShip* pEnemy2 = new PhasingEnemyShip();
		pEnemy2->SetTexture(pTexture);
		pEnemy2->SetCurrentLevel(this);
		pEnemy2->Initialize(position, (float)delay);
		AddGameObject(pEnemy2);
	}

		Boss01* pEnemy3 = new Boss01();
		pEnemy3->SetTexture(pBossTexture);
		pEnemy3->SetCurrentLevel(this);
		pEnemy3->Initialize(position, (float)delay);
		AddGameObject(pEnemy3);
	
	Level::LoadContent(pResourceManager);
}