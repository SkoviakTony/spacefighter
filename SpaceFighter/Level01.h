#pragma once

#include "Level.h"

class Level01 :	public Level
{

public:
	
	Level01() { }

	virtual ~Level01() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void UnloadContent() { }

private:

	// Load the level01 music
	ALLEGRO_SAMPLE* level01 = al_load_sample("..\\SpaceFighter\\Content\\Sounds\\level01.wav");
	ALLEGRO_SAMPLE_INSTANCE* level01_instance = al_create_sample_instance(level01);
};

