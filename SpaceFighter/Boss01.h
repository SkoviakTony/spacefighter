
#pragma once

#include "EnemyShip.h"

class Boss01 : public EnemyShip
{

public:

	Boss01();
	virtual ~Boss01() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

};