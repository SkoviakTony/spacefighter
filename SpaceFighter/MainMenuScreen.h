#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class MainMenuScreen : public MenuScreen
{

public:

	MainMenuScreen();

	virtual ~MainMenuScreen() { }

	virtual void LoadContent(ResourceManager *pResourceManager);
	
	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);


	virtual void SetQuitFlag() { m_isQuittingGame = true; }

	virtual bool IsQuittingGame() { return m_isQuittingGame; }

	virtual void SetStartFlag() { m_isStartingGame = true; }

	virtual bool IsStartingGame() { return m_isStartingGame;  }

	// Ideally starting and stopping music would have its own class, that's something I'd like to add if I had more time!
	virtual void StopMusic()
	{
		al_stop_sample_instance(mainmenu_instance);
		al_destroy_sample_instance(mainmenu_instance);
	}
	
private:
	
	Texture *m_pTexture;

	Vector2 m_texturePosition;

	bool m_isQuittingGame = false;
	bool m_isStartingGame = false;

	// Load the main menu music
	ALLEGRO_SAMPLE* mainmenu = al_load_sample("..\\SpaceFighter\\Content\\Sounds\\mainmenu.wav");
	ALLEGRO_SAMPLE_INSTANCE* mainmenu_instance = al_create_sample_instance(mainmenu);
};