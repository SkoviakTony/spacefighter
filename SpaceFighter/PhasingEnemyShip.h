
#pragma once

#include "EnemyShip.h"

class PhasingEnemyShip : public EnemyShip
{

public:

	PhasingEnemyShip();
	virtual ~PhasingEnemyShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

};