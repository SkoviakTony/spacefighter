
#include "Ship.h"


Ship::Ship()
{
	SetPosition(0, 0);
	SetCollisionRadius(10);

	m_speed = 300;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;

	Initialize();
}

void Ship::Update(const GameTime *pGameTime)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Update(pGameTime);
	}

	GameObject::Update(pGameTime);
}

void Ship::Hit(const float damage)
{
	ALLEGRO_SAMPLE* explosion = al_load_sample("..\\SpaceFighter\\Content\\Sounds\\explosion.wav");
	ALLEGRO_SAMPLE* impact = al_load_sample("..\\SpaceFighter\\Content\\Sounds\\impact.wav");
	if (!m_isInvulnurable)
	{
		m_hitPoints -= damage;
		al_play_sample(impact, 0.3, 0, 3, ALLEGRO_PLAYMODE_ONCE, NULL);
		if (m_hitPoints <= 0)
		{
			al_play_sample(explosion, 0.3, 0, 3, ALLEGRO_PLAYMODE_ONCE, NULL);
			//al_destroy_sample(explosion);
			GameObject::Deactivate();
		}
	}
}

void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

void Ship::FireWeapons(TriggerType type)
{
	m_weaponIt = m_weapons.begin();
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}